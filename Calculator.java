package com.JavaTutorial;

import java.io.*;
import java.util.*;

public class Calculator
{
    static double Addition(double number1, double number2)
    {
        return number1+number2;
    }

    static  double Subtraction(double number1, double number2)
    {
        return number1-number2;
    }

    static double Multiplication(double number1, double number2)
    {
        return number1 * number2;
    }

    static  double Division(double number1, double number2) throws Exception {
        if(number2==0)
        {
            throw new Exception("Divisor cannot be zero");
        }
        return number1/number2;
    }

    public static void main(String[] args)
    {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter your choice\n" +
                "1 for Addition\n" +
                "2 for Subtraction\n" +
                "3 for Multiplication\n" +
                "4 for Division");

        int choice = input.nextInt();

        double firstNumber = input.nextDouble();
        double secondNumber = input.nextDouble();


        double result = 0;
        switch (choice)
        {
            case 1:
                result = Addition(firstNumber,secondNumber);
                break;
            case 2:
                result = Subtraction(firstNumber,secondNumber);
                break;
            case 3:
                result = Multiplication(firstNumber,secondNumber);
                break;
            case 4:
                try
                {
                    result = Division(firstNumber,secondNumber);
                }
                catch (Exception e)
                {
                    System.out.println("Divisor cannot be a zero");
                }

                break;

            default:
                System.out.println("Enter a valid choice");
                break;
        }
        System.out.println("The result of the operation performed is "+result);
    }
}
