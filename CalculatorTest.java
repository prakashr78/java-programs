package com.JavaTutorial;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    private Calculator calc;
    @Before
    public void setUp() {
        calc = new Calculator();
    }
    @Test
    public void testAdd() {
        double a = 15;
        double b = 20;
        Object expectedResult = 35.0;
        Object result = calc.Addition(a, b);
        Assert.assertEquals(expectedResult,result);
    }
    @Test
    public void testSubtract() {
        double a = 25;
        double b = 20;
        Object expectedResult = 5.0;
        Object result = calc.Subtraction(a, b);
        assertEquals(expectedResult, result);;
    }
    @Test
    public void testMultiply() {
        double a = 10;
        double b = 25;
        Object expectedResult = 250.0;
        Object result = calc.Multiplication(a, b);
        assertEquals(expectedResult, result);
    }
    @Test
    public void testDivide() throws Exception {
        double a = 56;
        double b = 10;
        Object expectedResult = 5.6;
        Object result = calc.Division(a, b);
        assertEquals(expectedResult, result);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testDivideByZero() throws Exception {
        double a = 15;
        double b = 0;
        calc.Division(a, b);
    }
}